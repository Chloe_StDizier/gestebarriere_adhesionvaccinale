#-*- coding: utf-8 -*-
#!/usr/bin/env pyhton


from rpy2 import robjects
from rpy2.robjects.packages import importr
from rpy2.robjects import rl
import os
from rpy2.robjects.lib.dplyr import DataFrame
from rpy2.robjects.lib.dplyr import (summarize,group_by)


r=robjects.r

#IMPORT DES LIBRAIRIES R 

utils=importr('utils')

if robjects.packages.isinstalled('ggplot2')==False: 
	utils.install_packages('ggplot2')

if robjects.packages.isinstalled('tidyr')==False: 
	utils.install_packages('tidyr')

tidyr=importr('tidyr')
ggplot2=importr('ggplot2')

grdevices=importr('grDevices')

#IMPORT DES DONNEES

Gbarriere_adhesionVacc_src=r('Gbarriere_adhesionVacc_src<-read.csv2("geste_bar_vacc.csv",header=T,sep=";",dec=",")')


#CREATION D'UN DOSSIER POUR L'ENREGISTREMENT DES FICHIERS

if not os.path.exists('./graph'):
    os.makedirs('graph')


#DATAMANAGEMENT

#les données portant sur l'adhésion vaccinale n'étant disponible qu'à partir de la vague 19, nous ne prendrons pas en compte les données 
#relatives à la vague 17
#nous décidons également de nous intéresser uniquement aux adultes 

Gbarriere_adhesionVacc=r('Gbarriere_adhesionVacc_src[!(Gbarriere_adhesionVacc_src$semaine %in% c("vague 17 : 4-6 novembre",\
	"vague 18 : 23-25 novembre")),c(1:20)]')

print("nombre de ligne total : "+str(Gbarriere_adhesionVacc_src.nrow))
print("nombre de ligne a partir de la semaine 19 : "+str(Gbarriere_adhesionVacc.nrow)+"\n")
name=robjects.StrVector(['age','semaine','port_masque','port_masque_inf','port_masque_sup','lavage_main','lavage_main_inf',\
	'lavage_main_sup','eviter_regroup','eviter_regroup_inf','eviter_regroup_sup','eviter_serrer_main','eviter_serrer_main_inf',\
	'eviter_serrer_main_sup','aeration','aeration_inf','aeration_sup','adhesion_adulte','adhesion_adulte_inf','adhesion_adulte_sup'])                  
Gbarriere_adhesionVacc.colnames=name
print("nom des variables : ")
print(Gbarriere_adhesionVacc.colnames)
robjects.globalenv['Gbarriere_adhesionVacc']=Gbarriere_adhesionVacc



print("\n DESCRIPTION DES VARIABLES : \n")


for i in ("port_masque","lavage_main","eviter_regroup","eviter_serrer_main","adhesion_adulte","aeration"):  #parcours des variables
	print("\n******%s********"%i)
	
	#statistiques descriptives
	moyenne=r.mean(Gbarriere_adhesionVacc.rx2(i), **{'na.rm':'T'})
	ecart_type=r.sd(Gbarriere_adhesionVacc.rx2(i),**{'na.rm':'T'})
	interv=r.range(Gbarriere_adhesionVacc.rx2(i),**{'na.rm':'T'})
	mediane=r.median(Gbarriere_adhesionVacc.rx2(i),**{'na.rm':'T'})
	s='sum(is.na(Gbarriere_adhesionVacc$'+i+'))' 
	missing_data=r(s)
	pourc_missing_data=missing_data[0]*100/(Gbarriere_adhesionVacc.nrow)

	#affichage 
	print("pourcentage de données manquantes : "+str(pourc_missing_data)+" %")
	print("moyenne : "+str(moyenne[0]))
	print("écart-type : "+str(ecart_type[0]))
	print("range : [ "+str(interv[0])+" ; "+str(interv[1])+" ]")
	print("médiane : "+str(mediane[0]))
    

	#distribution des variables
	filename='graph/hist_'+i+'.png'
	grdevices.png(file=filename)
	var=Gbarriere_adhesionVacc.rx2(i)
	r('hist(%s, main="%s",xlab="",ylab="count")'%(var.r_repr(),i))
	grdevices.dev_off()


	#representation par semaine

	filename_sem='graph/'+i+'_par_sem.png' 
	grdevices.png(file=filename_sem,width=2000, height=896, res=150)
	cmd='g=ggplot(Gbarriere_adhesionVacc,aes(x=semaine, y='+i+',color=age))+geom_point(size=5,alpha=0.8)+ylab("'+i+' (%)")\
	+ggtitle("Respect de '+i+' en France")+theme_bw()+theme(axis.title.x = element_blank(),panel.grid.minor=element_blank(),\
	axis.line=element_line(colour="black"),plot.caption=element_text(color="grey",face="italic"),axis.text.x = element_text(angle = 60,hjust=1),\
	plot.title = element_text(size=14, face="bold",hjust=0.5))'
	r(cmd)
	g=r.g
	print(g)
	grdevices.dev_off()



#EVOLUTION DES VARIABLES

#agrégation des données par semaine et calcul des données 
agregated_data=(DataFrame(Gbarriere_adhesionVacc).
	group_by(Gbarriere_adhesionVacc.rx2('semaine')).
	summarize(AVG_port_masque=rl("mean(port_masque)"),AVG_lavage_main=rl("mean(lavage_main)"),AVG_eviter_regroup=rl('mean(eviter_regroup)'),\
		AVG_eviter_serrer_main=rl('mean(eviter_serrer_main)'),AVG_adhesion_adulte=rl('mean(adhesion_adulte)')))

agregated_data.colnames[0]='semaine' #nom de la colones des semaines 

robjects.globalenv['agregated_data']=agregated_data

#transposition du dataframe
agregated_data=r('gather(data=agregated_data,key=geste,value=moyenne,AVG_port_masque,AVG_lavage_main,AVG_eviter_regroup,AVG_eviter_serrer_main,\
	AVG_adhesion_adulte)')

robjects.globalenv['agregated_data']=agregated_data

#génération du fichier contenant le graphique
grdevices.png(file='./graph/evolution.png',width=2000, height=896, res=150)
g=r('ggplot(agregated_data,aes(x=semaine,y=moyenne,group=geste,color=geste))+geom_line()+geom_point()\
	+ggtitle("Evolution du respect des gestes barrières et de l\'adhésion vaccinale en France")\
	+theme(axis.title.x = element_blank(),panel.grid.minor=element_blank(),axis.line=element_line(colour="black"),\
	plot.caption=element_text(color="grey",face="italic"),axis.text.x = element_text(angle = 60,hjust=1),\
	plot.title = element_text(size=14, face="bold",hjust=0.5))+ylab("")')
print(g)
grdevices.dev_off()


#STATISTIQUES INFERENTIELLES

print("CORRELATION ENTRE GESTES BARRIERES ET ADHESION VACCINALE")

for i in ("port_masque","lavage_main","eviter_regroup","eviter_serrer_main"): #parcours des variables d'intérets
	#construction de la chaine de caractère correspondant à la requête R à exécuter
	s='cor.test(Gbarriere_adhesionVacc$'+i+',Gbarriere_adhesionVacc$adhesion_adulte,method="pearson")' 
	res=r(s) #exécution via R
	#affichage

	print('\n * test de corrélation entre '+i+' et l\' adhésion vaccinale adulte *')
	print("p-value : "+str((res[2][0])))
	print("taux de correlation : "+str((res[8][0])))

	#construction de la commande R pour la construction du graphique
	sg='ggplot(Gbarriere_adhesionVacc,aes(x=adhesion_adulte,y='+i+'))+geom_point()+geom_smooth(method=lm,se=T,col="red")\
	+ggtitle("'+i+' ~ adhesion_adulte")+theme_bw()+theme(panel.grid.minor=element_blank(),axis.line=element_line(colour="black"),\
	plot.caption=element_text(color="grey",face="italic"),plot.title = element_text(size=14, face="bold",hjust=0.5))'
	
	#construction du nom de fichier
	filename='graph/adhesion_'+i+'.png'

	#génération du fichier contenant le grahique
	grdevices.png(file=filename)
	g=r(sg)
	print(g)
	grdevices.dev_off()


