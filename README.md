# GesteBarriere_AdhesionVaccinale



## Introduction au projet

### Contexte 
Depuis janvier 2020, la France est touchée par la pandémie de Covid-19, depuis cette date, différentes mesures sont prises successivement par le gouvernement français, en particulier, depuis le premier confinement du 17 mars 2020. Des gestes barrières sont mis en place au sein de la population afin de limiter la propagation du virus avec des réglementations plus ou moins strictes selon l’évolution de la crise sanitaire. L’un des gestes principaux est le port du masque qui est encore à ce jour obligatoire dans tous les lieux publics fermés et en extérieur sous certaines conditions. On retrouve également d’autres mesures plus personnelles comme le lavage des mains ou l’aération du logement.
Après presque une année de pandémie, la campagne de vaccination contre la Covid-19 a été lancé en France le 27 décembre 2020. Néanmoins, le vaccin ne gagne pas immédiatement la confiance de tous et on observe encore aujourd’hui la réticence d’une partie des Français.

### Données 

Depuis le 23 mars 2020, Santé Public France a lancé l’enquête CoviPrev pour suivre l’évolution des comportements de la population au cours de la pandémie. Elle se présente sous forme de questionnaire en ligne soumis de manière répétée par vague, à des échantillons indépendants de 2000 personnes. Les participants sont des résidents de France métropolitaine âgée de plus de 18 ans recrutés par access panel. L’échantillonnage est réalisé par quotas (sexe, âge, catégorie socio-professionnelle …) redressé sur le recensement général de la population 2016.
Ainsi, les données mesurant la connaissance et la mise en oeuvre des gestes barrières suivants : port du masque, éviter les regroupements, saluer sans se serrer la main, aérer son logement, se laver les mains régulièrement sont récoltés. Les participants sont également questionnés sur leur intention de vaccination pour eux même à partir de décembre 2020 (vague 19 de l’enquête) mais également pour leurs enfants à partir de juin 2021. Dès le lancement de la campagne de vaccination, la question de la vaccination est ajoutée à l’enquête.
Dans cette étude, seront utilisées les données relatives à cette enquête de la vague 17 (4-6 novembre 2020) à la vague 28 (28 septembre – 5 octobre 2021). De plus, les données portant sur les enfants ne seront pas prises en compte.


### Problématique et hypothèses

L’étude menée ici s’intéresse alors au lien entre respect des différents gestes barrières et adhésion vaccinale chez les adultes. Dans le cadre de cette dernière, la problématique élaborée est la suivante : le respect des gestes barrières est-il influencé par l’adhésion vaccinale chez les adultes ? En d’autres termes, cette problématique vise à savoir si la tendance de la population adulte à adhérer à la vaccination joue un rôle sur le respect des gestes barrières suivants : le port du masque, le lavage régulier des mains, les salutations sans se serrer la main, l’aération des logements et l’évitement des regroupements.


## Matériel et méthodes

Les données utilisées sont disponibles à l'adresse suivante : https://www.santepubliquefrance.fr/etudes-et-enquetes/coviprev-une-enquete-pour-suivre-l-evolution-des-comportements-et-de-la-sante-mentale-pendant-l-epidemie-de-covid-19

L'étude a été réalisé avec Python 3.9 en utilisant principalement la libraire rpy2 (v. 3.4.5).

Le dossier 'graph' contient toutes les figures générée lors de l'exécution du script disponible dans la section 'script_et_donnees' sur les données présentes au même emplacment.
Un compte rendu des analyses effectuées est disponible dans le fichier Rapport.pdf


## Auteurs 

Ce projet a été réalisé par Amine Sidi Boulenouar, Lauryn Noumbissi Kapawa et Chloé Saint-Dizier dans le cadre du Master 1 Data Science en Santé (ILIS - Université de Lille)
